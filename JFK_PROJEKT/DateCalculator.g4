grammar DateCalculator;

fragment SINGLE_SPACE   : '\u0020'; // ' '
fragment TABULATION     : '\u0009'; // '\t'
fragment LINE_FEED      : '\u000A'; // '\n'
fragment CARRAIGE_RETURN: '\u000D'; // '\r'

Dot           : '.';
Add           : '+';
Subtract      : '-';
Slash		  : '/';
Separator	  : ':';

WhiteSpace    : ( SINGLE_SPACE | TABULATION )+ -> skip;
NewLine       : ( CARRAIGE_RETURN | LINE_FEED )+ -> skip;

Zero_dwaczt : '0'[0-9]
		| '1'[0-9]
		| '2'[0-4]
		;

DZIEN	: '0'[1-9]
		| [1-2][0-9]
		| '3'[0-1]
		;

MIESIAC	: 'STY'
		| 'LUT'
		| 'MAR'
		| 'KWI'
		| 'MAJ'
		| 'CZE'
		| 'LIP'
		| 'SIE'
		| 'WRZ'
		| 'PAZ'
		| 'LIS'
		| 'GRU'
		;

ROK : [0-9][0-9][0-9][0-9]; 

MINUTY 	: [0-5][0-9];

LICZBA_DNI : [0-9]+;

expr
:   expr op=(Add|Subtract) ( date | datetime | timespan )
| ( date | datetime | timespan )
;
		
datetime	: date godziny ( Separator | Dot ) minuty ( Separator | Dot ) sekundy;
date		: dzien (Dot | Slash) miesiac (Dot|Slash) rok;
timespan	: ( liczba_dni Dot )? godziny_timespan ( Separator | Dot) minuty_timespan ( Separator | Dot ) sekundy_timespan;
dzien		: DZIEN | Zero_dwaczt;
liczba_dni	: LICZBA_DNI | dzien | MINUTY | ROK;
miesiac 	: MIESIAC;
rok			: ROK;
godziny 	: Zero_dwaczt;
godziny_timespan	: LICZBA_DNI | dzien | MINUTY | ROK;
minuty		: Zero_dwaczt | DZIEN | MINUTY;
minuty_timespan		: Zero_dwaczt | DZIEN | MINUTY | LICZBA_DNI;
sekundy		: Zero_dwaczt | DZIEN | MINUTY;
sekundy_timespan		: Zero_dwaczt | DZIEN | MINUTY | LICZBA_DNI;
