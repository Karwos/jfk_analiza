﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JFK_PROJEKT
{
    public class  Time
    {
        public DateTime datetime { get; set; }
        public TimeSpan timespan { get; set; }
        public bool isTimeSpan { get; set; }

        public override string ToString()
        {
            if (isTimeSpan)
                return timespan.ToString();
            else
                return datetime.ToString();
        }

        public static Time operator +(Time a, Time b)
        {
            Time time = new Time();

            if(a.isTimeSpan && b.isTimeSpan)
            {
                time.timespan = a.timespan.Add(b.timespan);
                time.isTimeSpan = true;
            }
            else if(a.isTimeSpan && !b.isTimeSpan)
            {
                time.datetime =b.datetime.Add(a.timespan);
                time.isTimeSpan = false;
            }
            else if(!a.isTimeSpan && b.isTimeSpan)
            {
                time.datetime = a.datetime.Add(b.timespan);
                time.isTimeSpan = false;
            }
            else if(!a.isTimeSpan && !b.isTimeSpan)
            {
                throw new InvalidOperationException();
            }
            return time;
        }

        public static Time operator -(Time a, Time b)
        {
            Time time = new Time();

            if (a.isTimeSpan && b.isTimeSpan)
            {
                time.timespan = a.timespan.Subtract(b.timespan);
                time.isTimeSpan = true;
            }
            else if (a.isTimeSpan && !b.isTimeSpan)
            {
                throw new InvalidOperationException();
            }
            else if (!a.isTimeSpan && b.isTimeSpan)
            {
                time.datetime = a.datetime.Subtract(b.timespan);
                time.isTimeSpan = false;
            }
            else if (!a.isTimeSpan && !b.isTimeSpan)
            {
                time.timespan = a.datetime.Subtract(b.datetime);
                time.isTimeSpan = true;
            }
            return time;
        }
    }
}
